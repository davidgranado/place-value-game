/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
	mode: 'development',
	entry: [
		'./src/index.ts',
	],
	resolve: {
		extensions: [ '.ts', '.js' ],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new CopyPlugin([
			{
				from: './src/assets',
				to: 'assets',
			},
		]),
		new HtmlWebpackPlugin({
			title: 'Development',
			template: './src/index.html',
		}),
	],
	devServer: {
		contentBase: path.join(__dirname, 'build'),
		compress: true,
		port: 9000,
	},
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'build'),
	},

	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [
					'file-loader',
				],
			},
		],
	},
};
