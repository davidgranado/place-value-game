import { Application, Sprite, Texture } from 'pixi.js';

const app = new Application({
	view: document.getElementById('screen') as HTMLCanvasElement,
	width: window.innerWidth,
	height: window.innerHeight,
});

const ship = new Sprite(
	Texture.from('assets/images/alien1.png'),
);
const shield = new Sprite(
	Texture.from('assets/images/shield.png'),
);

shield.anchor.x = .5;
shield.anchor.y = .5;

ship.addChild(shield);

ship.x = app.screen.width / 2;
ship.y = app.screen.height / 2;
ship.anchor.x = 0.5;
ship.anchor.y = 0.5;
app.stage.addChild(ship);

let up = false;
const rate = .04;

app.ticker.add(() => {
	const newAlpha = shield.alpha + (up ? rate : -rate);

	console.log(up, newAlpha);

	if(up && newAlpha >= 1) {
		up = false;
	} else if(!up && newAlpha <= 0) {
		up = true;
		shield.angle = Math.random() * 360;
	} else {
		shield.alpha = newAlpha;
	}
});
